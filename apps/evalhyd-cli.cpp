#include <CLI/CLI.hpp>

#include "evalhyd-cli/evald.hpp"
#include "evalhyd-cli/evalp.hpp"

/// Main program implementing the Command Line Interface.
int main(int argc, char **argv) {
    // instantiate application
    CLI::App app {"Utility to evaluate streamflow predictions"};

    // add option to configure through TOML file
    app.set_config("--config", "config.toml",
                   "Path to TOML file to configure the subcommands");

    // set up subcommand for deterministic evaluation
    setup_evald(app);

    // set up subcommand for probabilistic evaluation
    setup_evalp(app);

    // parse command line arguments
    // (/!\ overwrite config files arguments)
    CLI11_PARSE(app, argc, argv);

    return 0;
}
