#ifndef EVALHYD_CLI_EVALP_HPP
#define EVALHYD_CLI_EVALP_HPP

#include "CLI/CLI.hpp"

void setup_evalp(CLI::App &app);

#endif //EVALHYD_CLI_EVALP_HPP
