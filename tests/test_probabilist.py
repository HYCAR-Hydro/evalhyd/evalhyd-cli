import unittest
import subprocess
import numpy
import os
import tempfile


_obs = "./data/evalp/obs"
_prd = "./data/evalp/prd"
_thr = "./data/evalp/thr"
_events = "high"
_lvl = ("30.", "80.")
_msk = "./data/evalp/msk"
_cdt = "./data/evalp/cdt"

# list all available probabilistic metrics
_all_metrics = (
    # threshold-based
    'BS', 'BSS', 'BS_CRD', 'BS_LBD', 'REL_DIAG', 'CRPS_FROM_BS',
    # CDF-based
    'CRPS_FROM_ECDF',
    # quantile-based
    'QS', 'CRPS_FROM_QS',
    # contingency table-based
    'CONT_TBL', 'POD', 'POFD', 'FAR', 'CSI', 'ROCSS',
    # ranks-based
    'RANK_HIST', 'DS', 'AS',
    # intervals-based
    'CR', 'AW', 'AWN', 'WS',
    # multivariate
    'ES'
)

# list all available deterministic diagnostics
_all_diags = (
    'completeness'
)


class TestMetrics(unittest.TestCase):

    expected_thr = {
        metric: (
            numpy.genfromtxt(f"./expected/evalp/{metric}.csv", delimiter=',')
            [numpy.newaxis, numpy.newaxis, numpy.newaxis, numpy.newaxis, ...]
        ) for metric in ('BS', 'BSS', 'BS_CRD', 'BS_LBD', 'REL_DIAG', 'CRPS_FROM_BS')
    }
    # /!\ stacked-up thresholds in CSV file for REL_DIAG
    #     because 7D metric so need to reshape array
    expected_thr['REL_DIAG'] = (
        expected_thr['REL_DIAG'].reshape(expected_thr['BS'].shape + (52, 3))
    )

    expected_qtl = {
        metric: (
            numpy.genfromtxt(f"./expected/evalp/{metric}.csv", delimiter=',')
            [numpy.newaxis, numpy.newaxis, numpy.newaxis, numpy.newaxis, ...]
        ) for metric in ('QS', 'CRPS_FROM_QS')
    }

    expected_ct = {
        metric: (
            numpy.genfromtxt(f"./expected/evalp/{metric}.csv", delimiter=',')
            [numpy.newaxis, numpy.newaxis, numpy.newaxis, numpy.newaxis, ...]
        ) for metric in ('CONT_TBL', 'POD', 'POFD', 'FAR', 'CSI', 'ROCSS')
    }
    # /!\ stacked-up thresholds in CSV file for CONT_TBL
    #     because 7D metric so need to reshape array
    expected_ct['CONT_TBL'] = (
        expected_ct['CONT_TBL'].reshape(expected_ct['POD'].shape + (4,))
    )

    expected_rk = {
        metric: (
            numpy.genfromtxt(f"./expected/evalp/{metric}.csv", delimiter=',')
            [numpy.newaxis, numpy.newaxis, numpy.newaxis, numpy.newaxis, ...]
        ) for metric in ('RANK_HIST', 'DS', 'AS')
    }

    expected_itv = {
        metric: (
            numpy.genfromtxt(f"./expected/evalp/{metric}.csv", delimiter=',')
            [numpy.newaxis, numpy.newaxis, numpy.newaxis, numpy.newaxis, ...]
        ) for metric in ('CR', 'AW', 'AWN', 'WS')
    }

    expected_mvr = {
        metric: (
            numpy.genfromtxt(f"./expected/evalp/{metric}.csv", delimiter=',')
            [numpy.newaxis, numpy.newaxis, numpy.newaxis, numpy.newaxis, ...]
        ) for metric in ('ES',)
    }

    def test_thresholds_metrics_to_file(self):
        # clean up outputs directory to make sure
        # it is checking newly created files
        subprocess.run(['rm -rf ./outputs/*/*.csv'], shell=True)

        # compute all metrics with evalhyd evald
        subprocess.run(
            ['evalhyd', 'evalp', _obs, _prd,
             *tuple(self.expected_thr.keys()),
             '--q_thr', _thr, '--events', 'high',
             '--to_file', '--out_dir', './outputs']
        )

        # read in and check data in CSV files
        for metric in self.expected_thr.keys():
            for lt in ['leadtime_1', 'leadtime_2']:
                for st in ['site_a', 'site_b', 'site_c']:
                    res = numpy.genfromtxt(
                        f"./outputs/{lt}/{st}_{metric}.csv", delimiter=','
                    )
                    if metric == 'REL_DIAG':
                        # /!\ stacked-up thresholds in CSV file for REL_DIAG
                        #     because 7D metric so need to reshape array
                        res = res.reshape(self.expected_thr['REL_DIAG'].shape[4:])

                    with self.subTest(metric=metric, leadtime=lt, site=st):
                        numpy.testing.assert_almost_equal(
                            res, self.expected_thr[metric][0, 0, 0, 0],
                            decimal=(3 if metric == 'CRPS_FROM_BS' else 6)
                        )

    def test_quantiles_metrics_to_file(self):
        # clean up outputs directory to make sure
        # it is checking newly created files
        subprocess.run(['rm -rf ./outputs/*/*.csv'], shell=True)

        # compute all metrics with evalhyd evald
        subprocess.run(
            ['evalhyd', 'evalp', _obs, _prd,
             *tuple(self.expected_qtl.keys()),
             '--to_file', '--out_dir', './outputs']
        )

        # read in and check data in CSV files
        for metric in self.expected_qtl.keys():
            for lt in ['leadtime_1', 'leadtime_2']:
                for st in ['site_a', 'site_b', 'site_c']:
                    res = numpy.genfromtxt(
                        f"./outputs/{lt}/{st}_{metric}.csv", delimiter=','
                    )

                    with self.subTest(metric=metric, leadtime=lt, site=st):
                        numpy.testing.assert_almost_equal(
                            res, self.expected_qtl[metric][0, 0, 0, 0],
                            decimal=3
                        )

    def test_contingency_table_metrics_to_file(self):
        # clean up outputs directory to make sure
        # it is checking newly created files
        subprocess.run(['rm -rf ./outputs/*/*.csv'], shell=True)

        # compute all metrics with evalhyd evald
        subprocess.run(
            ['evalhyd', 'evalp', _obs, _prd,
             *tuple(self.expected_ct.keys()),
             '--q_thr', _thr, '--events', 'low',
             '--to_file', '--out_dir', './outputs']
        )

        # read in and check data in CSV files
        for metric in self.expected_ct.keys():
            for lt in ['leadtime_1', 'leadtime_2']:
                for st in ['site_a', 'site_b', 'site_c']:
                    res = numpy.genfromtxt(
                        f"./outputs/{lt}/{st}_{metric}.csv", delimiter=','
                    )
                    if metric == 'CONT_TBL':
                        # /!\ stacked-up thresholds in CSV file for CONT_TBL
                        #     because 7D metric so need to reshape array
                        res = res.reshape(self.expected_ct['CONT_TBL'].shape[4:])

                    with self.subTest(metric=metric, leadtime=lt, site=st):
                        numpy.testing.assert_almost_equal(
                            res, self.expected_ct[metric][0, 0, 0, 0],
                            decimal=6
                        )

    def test_ranks_metrics_to_file(self):
        # subset input data set to include only one site and one lead time
        # because due the random nature of the ranks-base metrics, the problem
        # shape has an impact on the random tensor generated, and expected
        # output files are based on a mono-site mono-leadtime problem
        with tempfile.TemporaryDirectory() as tmp_dir:
            obs = os.path.join(tmp_dir, 'obs')
            prd = os.path.join(tmp_dir, 'prd')
            if not os.path.exists(obs):
                os.makedirs(obs)
            if not os.path.exists(os.path.join(prd, 'leadtime_1')):
                os.makedirs(os.path.join(prd, 'leadtime_1'))

            numpy.savetxt(
                os.path.join(obs, 'site_a.csv'),
                numpy.genfromtxt(
                    os.path.join(_obs, 'site_a.csv'), delimiter=','
                )[numpy.newaxis, :],
                delimiter=','
            )
            numpy.savetxt(
                os.path.join(prd, 'leadtime_1', 'site_a.csv'),
                numpy.genfromtxt(
                    os.path.join(_prd, 'leadtime_1', 'site_a.csv'), delimiter=','
                )[:, :],
                delimiter=','
            )

            # clean up outputs directory to make sure
            # it is checking newly created files
            subprocess.run(['rm -rf ./outputs/*/*.csv'], shell=True)

            # compute all metrics with evalhyd evald
            subprocess.run(
                ['evalhyd', 'evalp', obs, prd,
                 *tuple(self.expected_rk.keys()),
                 '--seed', '7', '--to_file', '--out_dir', './outputs']
            )

            # read in and check data in CSV files
            for metric in self.expected_rk.keys():
                res = numpy.genfromtxt(
                    f"./outputs/leadtime_1/site_a_{metric}.csv", delimiter=','
                )

                with self.subTest(metric=metric):
                    numpy.testing.assert_almost_equal(
                        res, self.expected_rk[metric][0, 0, 0, 0],
                        decimal=(2 if metric == 'DS' else 6)
                    )

    def test_intervals_metrics_to_file(self):
        # clean up outputs directory to make sure
        # it is checking newly created files
        subprocess.run(['rm -rf ./outputs/*/*.csv'], shell=True)

        # compute all metrics with evalhyd evald
        subprocess.run(
            ['evalhyd', 'evalp', _obs, _prd,
             *tuple(self.expected_itv.keys()),
             '--c_lvl', *_lvl,
             '--to_file', '--out_dir', './outputs']
        )

        # read in and check data in CSV files
        for metric in self.expected_itv.keys():
            for lt in ['leadtime_1', 'leadtime_2']:
                for st in ['site_a', 'site_b', 'site_c']:
                    res = numpy.genfromtxt(
                        f"./outputs/{lt}/{st}_{metric}.csv", delimiter=','
                    )

                    with self.subTest(metric=metric, leadtime=lt, site=st):
                        numpy.testing.assert_almost_equal(
                            res, self.expected_itv[metric][0, 0, 0, 0],
                            decimal=(2 if metric == 'WS' else
                                     4 if metric == 'AW' else
                                     6)
                        )

    def test_multivariate_metrics_to_file(self):
        # clean up outputs directory to make sure
        # it is checking newly created files
        subprocess.run(['rm -rf ./outputs/*/*.csv'], shell=True)

        # compute all metrics with evalhyd evald
        subprocess.run(
            ['evalhyd', 'evalp', _obs, _prd,
             *tuple(self.expected_mvr.keys()),
             '--to_file', '--out_dir', './outputs']
        )

        # read in and check data in CSV files
        for metric in self.expected_mvr.keys():
            for lt in ['leadtime_1', 'leadtime_2']:
                res = numpy.genfromtxt(
                    f"./outputs/{lt}/multisite_{metric}.csv", delimiter=','
                )

                with self.subTest(metric=metric, leadtime=lt):
                    numpy.testing.assert_almost_equal(
                        res, self.expected_mvr[metric][0, 0, 0, 0],
                        decimal=(3 if metric == 'ES' else 6)
                    )


class TestMasking(unittest.TestCase):

    def test_masks_vs_conditions(self):
        self.assertEqual(
            subprocess.run(
                ['evalhyd', 'evalp', _obs, _prd, *_all_metrics,
                 '--q_thr', _thr, '--events', _events, '--c_lvl', *_lvl,
                 '--seed', '7', '--t_msk', _msk],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8'),
            subprocess.run(
                ['evalhyd', 'evalp', _obs, _prd, *_all_metrics,
                 '--q_thr', _thr, '--events', _events, '--c_lvl', *_lvl,
                 '--seed', '7', '--m_cdt', _cdt],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8')
        )


class TestInterface(unittest.TestCase):

    def test_inline_vs_config_file(self):
        self.assertEqual(
            # passing command arguments inline
            subprocess.run(
                ['evalhyd', 'evalp', _obs, _prd,
                 *_all_metrics,
                 '--q_thr', _thr, '--events', _events,
                 '--c_lvl', *_lvl, '--seed', '7',
                 '--t_msk', _msk],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8'),
            # passing command arguments via TOML config file
            subprocess.run(
                ['evalhyd', '--config', './config/evalp.toml', 'evalp'],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8')
        )


class TestUncertainty(unittest.TestCase):

    def test_bootstrap(self):
        for metric in _all_metrics:
            # compute each metric in turn with evalhyd evald
            # and check console output
            with self.subTest(metric=metric):
                # skip ranks-based metrics because they contain a random element
                if metric in ("RANK_HIST", "DS", "AS"):
                    continue

                self.assertEqual(
                    # use one year of data through the year block bootstrapping
                    subprocess.run(
                        ['evalhyd', 'evalp',
                         './data/evalp/obs_1yr', './data/evalp/prd_1yr',
                         metric, '--q_thr', _thr, '--events', _events,
                         '--c_lvl', *_lvl,
                         '--bootstrap', 'n_samples', '1', 'len_sample', '3',
                         'summary', '0', '--dts', './data/evalp/dts.csv'],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8'),
                    # use three years of data (same year repeated three times)
                    subprocess.run(
                        ['evalhyd', 'evalp',
                         './data/evalp/obs_3yrs',
                         './data/evalp/prd_3yrs',
                         metric, '--q_thr', _thr, '--events', _events,
                         '--c_lvl', *_lvl],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8')
                )


class TestDiagnostics(unittest.TestCase):

    def test_completeness(self):
        obs = numpy.array(
            [[4.7, 4.3, numpy.nan, 2.7, 4.1, 5.0]]
        )

        prd = numpy.array(
            [[[[5.3, numpy.nan, 5.7, 2.3, 3.3, numpy.nan],
               [4.3, numpy.nan, 4.7, 4.3, 3.4, numpy.nan],
               [5.3, numpy.nan, 5.7, 2.3, 3.8, numpy.nan]],
              [[numpy.nan, 4.2, 5.7, 2.3, 3.1, 4.1],
               [numpy.nan, 4.2, 4.7, 4.3, 3.3, 2.8],
               [numpy.nan, 5.2, 5.7, 2.3, 3.9, 3.5]]]]
        )

        msk = numpy.array(
            [[[[True, True, True, False, True, True],
               [True, True, True, True, True, True]],
              [[True, True, True, True, True, False],
               [True, True, True, True, True, True]]]]
        )

        with tempfile.TemporaryDirectory() as tmp_dir:
            obs_dir = os.path.join(tmp_dir, 'obs')
            if not os.path.exists(obs_dir):
                os.makedirs(obs_dir)
            numpy.savetxt(
                os.path.join(obs_dir, 'site_a.csv'),
                obs,
                delimiter=','
            )

            prd_dir = os.path.join(tmp_dir, 'prd')
            msk_dir = os.path.join(tmp_dir, 'msk')
            for n in [1, 2]:
                if not os.path.exists(os.path.join(prd_dir, f'leadtime_{n}')):
                    os.makedirs(os.path.join(prd_dir, f'leadtime_{n}'))
                if not os.path.exists(os.path.join(msk_dir, f'leadtime_{n}')):
                    os.makedirs(os.path.join(msk_dir, f'leadtime_{n}'))

                numpy.savetxt(
                    os.path.join(prd_dir, f'leadtime_{n}', 'site_a.csv'),
                    prd[0, n-1],
                    delimiter=','
                )
                numpy.savetxt(
                    os.path.join(msk_dir, f'leadtime_{n}', 'site_a.csv'),
                    msk[0, n-1],
                    delimiter=','
                )

            exp = ("{{{{ 0.246875},\n"
                   "   { 0.239583}},\n"
                   "  {{ 0.204167},\n"
                   "   { 0.323437}}}}\n"
                   "{{{{ 2.},\n"
                   "   { 3.}},\n"
                   "  {{ 3.},\n"
                   "   { 4.}}}}\n")

            res = subprocess.run(
                ['evalhyd', 'evalp', obs_dir, prd_dir, 'CRPS_FROM_QS',
                 '--t_msk', msk_dir, '--diagnostics', 'completeness'],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8').replace('\r\n', '\n')

            self.assertEqual(res, exp)


if __name__ == '__main__':
    test_loader = unittest.TestLoader()
    test_suite = unittest.TestSuite()

    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestMetrics)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestMasking)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestInterface)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestUncertainty)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestDiagnostics)
    )

    runner = unittest.TextTestRunner(verbosity=2)
    result = runner.run(test_suite)

    if not result.wasSuccessful():
        exit(1)
