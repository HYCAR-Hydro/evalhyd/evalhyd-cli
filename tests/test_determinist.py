import unittest
import subprocess
import numpy
from tempfile import NamedTemporaryFile


# path to input data files
_obs = "./data/evald/obs.csv"
_prd = "./data/evald/prd.csv"
_thr = "./data/evald/thr.csv"
_events = "high"
_msk = "./data/evald/msk.csv"
_cdt = "./data/evald/cdt.csv"

# list all available deterministic metrics
_all_metrics = (
    # errors-based
    'MAE', 'MARE', 'MSE', 'RMSE',
    # efficiencies-based
    'NSE', 'KGE', 'KGE_D', 'KGEPRIME', 'KGEPRIME_D', 'KGENP', 'KGENP_D',
    # contingency table-based
    'CONT_TBL'
)

# list all available deterministic diagnostics
_all_diags = (
    'completeness'
)


class TestMetrics(unittest.TestCase):

    expected = {
        metric: (
            numpy.genfromtxt(f"./expected/evald/{metric}.csv", delimiter=',')
            [:, numpy.newaxis, numpy.newaxis]
        ) for metric in _all_metrics
    }
    # /!\ stacked-up thresholds in CSV file for CONT_TBL
    #     because 5D metric so need to reshape array
    expected['CONT_TBL'] = (
        expected['CONT_TBL'].reshape(expected['NSE'].shape + (4, 4))
    )

    def test_metrics_to_file(self):
        # clean up outputs directory to make sure
        # it is checking newly created files
        subprocess.run(['rm -f ./outputs/*.csv'], shell=True)

        # compute all metrics with evalhyd evald
        subprocess.run(
            ['evalhyd', 'evald', _obs, _prd,
             *tuple(self.expected.keys()),
             '--q_thr', _thr, '--events', _events,
             '--to_file', '--out_dir', './outputs']
        )

        # read in and check data in CSV files
        for metric in self.expected.keys():
            res = numpy.genfromtxt(
                f"./outputs/evald_{metric}.csv", delimiter=','
            )[:, numpy.newaxis, numpy.newaxis]
            if metric == 'CONT_TBL':
                # /!\ stacked-up thresholds in CSV file for CONT_TBL
                #     because 5D metric so need to reshape array
                res = res.reshape(self.expected['CONT_TBL'].shape)

            with self.subTest(metric=metric):
                numpy.testing.assert_almost_equal(
                    res, self.expected[metric],
                    decimal=(0 if metric in ("MSE")
                             else 3 if metric in ("RMSE", "MAE")
                             else 5)
                )


class TestTransform(unittest.TestCase):

    obs = numpy.genfromtxt(_obs, delimiter=',')[numpy.newaxis, :]
    prd = numpy.genfromtxt(_prd, delimiter=',')[:, :]
    thr = numpy.genfromtxt(_thr, delimiter=',')[:, :]

    def test_transform_sqrt(self):

        obs_trf = NamedTemporaryFile()
        numpy.savetxt(obs_trf.name, self.obs ** 0.5, delimiter=',')
        prd_trf = NamedTemporaryFile()
        numpy.savetxt(prd_trf.name, self.prd ** 0.5, delimiter=',')
        thr_trf = NamedTemporaryFile()
        numpy.savetxt(thr_trf.name, self.thr ** 0.5, delimiter=',')

        for metric in _all_metrics:
            with self.subTest(metric=metric):
                self.assertEqual(
                    subprocess.run(
                        ['evalhyd', 'evald', _obs, _prd, metric,
                         '--transform', 'sqrt',
                         '--q_thr', _thr, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8'),
                    subprocess.run(
                        ['evalhyd', 'evald', obs_trf.name, prd_trf.name, metric,
                         '--q_thr', thr_trf.name, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8')
                )

        obs_trf.close()
        prd_trf.close()
        thr_trf.close()

    def test_transform_inv(self):

        eps = 0.01 * numpy.mean(self.obs)

        obs_trf = NamedTemporaryFile()
        numpy.savetxt(obs_trf.name, 1 / (self.obs + eps), delimiter=',')
        prd_trf = NamedTemporaryFile()
        numpy.savetxt(prd_trf.name, 1 / (self.prd + eps), delimiter=',')
        thr_trf = NamedTemporaryFile()
        numpy.savetxt(thr_trf.name, 1 / (self.thr + eps), delimiter=',')

        for metric in _all_metrics:
            with self.subTest(metric=metric):
                self.assertEqual(
                    subprocess.run(
                        ['evalhyd', 'evald', _obs, _prd, metric,
                         '--transform', 'inv',
                         '--q_thr', _thr, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8'),
                    subprocess.run(
                        ['evalhyd', 'evald',
                         obs_trf.name, prd_trf.name, metric,
                         '--q_thr', thr_trf.name, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8')
                )

        obs_trf.close()
        prd_trf.close()
        thr_trf.close()

    def test_transform_log(self):

        eps = 0.01 * numpy.mean(self.obs)

        obs_trf = NamedTemporaryFile()
        numpy.savetxt(obs_trf.name, numpy.log(self.obs + eps), delimiter=',')
        prd_trf = NamedTemporaryFile()
        numpy.savetxt(prd_trf.name, numpy.log(self.prd + eps), delimiter=',')
        thr_trf = NamedTemporaryFile()
        numpy.savetxt(thr_trf.name, numpy.log(self.thr + eps), delimiter=',')

        for metric in _all_metrics:
            with self.subTest(metric=metric):
                self.assertEqual(
                    subprocess.run(
                        ['evalhyd', 'evald', _obs, _prd, metric,
                         '--transform', 'log',
                         '--q_thr', _thr, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8'),
                    subprocess.run(
                        ['evalhyd', 'evald',
                         obs_trf.name, prd_trf.name, metric,
                         '--q_thr', thr_trf.name, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8')
                )

        obs_trf.close()
        prd_trf.close()
        thr_trf.close()

    def test_transform_pow(self):

        obs_trf = NamedTemporaryFile()
        numpy.savetxt(obs_trf.name, self.obs ** 0.3, delimiter=',')
        prd_trf = NamedTemporaryFile()
        numpy.savetxt(prd_trf.name, self.prd ** 0.3, delimiter=',')
        thr_trf = NamedTemporaryFile()
        numpy.savetxt(thr_trf.name, self.thr ** 0.3, delimiter=',')

        for metric in _all_metrics:
            with self.subTest(metric=metric):
                self.assertEqual(
                    subprocess.run(
                        ['evalhyd', 'evald', _obs, _prd, metric,
                         '--transform', 'pow', '--exponent', '0.3',
                         '--q_thr', _thr, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8'),
                    subprocess.run(
                        ['evalhyd', 'evald', obs_trf.name, prd_trf.name, metric,
                         '--q_thr', thr_trf.name, '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8')
                )

        obs_trf.close()
        prd_trf.close()
        thr_trf.close()


class TestMasking(unittest.TestCase):

    def test_masks_vs_conditions(self):
        self.assertEqual(
            subprocess.run(
                ['evalhyd', 'evald', _obs, _prd, *_all_metrics,
                 '--q_thr', _thr, '--events', _events,
                 '--t_msk', _msk],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8'),
            subprocess.run(
                ['evalhyd', 'evald', _obs, _prd, *_all_metrics,
                 '--q_thr', _thr, '--events', _events,
                 '--m_cdt', _cdt],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8')
        )


class TestInterface(unittest.TestCase):

    def test_inline_vs_config_file(self):
        self.assertEqual(
            # passing command arguments inline
            subprocess.run(
                ['evalhyd', 'evald', _obs, _prd,
                 *_all_metrics,
                 '--transform', 'pow', '--exponent', '0.3',
                 '--q_thr', _thr, '--events', _events],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8'),
            # passing command arguments via TOML config file
            subprocess.run(
                ['evalhyd', '--config', './config/evald.toml', 'evald'],
                stdout=subprocess.PIPE
            ).stdout.decode('utf-8')
        )


class TestUncertainty(unittest.TestCase):

    def test_bootstrap(self):
        for metric in _all_metrics:
            # compute each metric in turn with evalhyd evald
            # and check console output
            with self.subTest(metric=metric):
                self.assertEqual(
                    # use one year of data through the year block bootstrapping
                    subprocess.run(
                        ['evalhyd', 'evald',
                         './data/evald/obs_1yr.csv', './data/evald/prd_1yr.csv',
                         metric,
                         '--q_thr', './data/evald/thr_1yr_3yrs.csv',
                         '--events', _events,
                         '--bootstrap', 'n_samples', '1', 'len_sample', '3',
                         'summary', '0', '--dts', './data/evald/dts.csv'],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8'),
                    # use three years of data (same year repeated three times)
                    subprocess.run(
                        ['evalhyd', 'evald',
                         './data/evald/obs_3yrs.csv',
                         './data/evald/prd_3yrs.csv',
                         metric,
                         '--q_thr', './data/evald/thr_1yr_3yrs.csv',
                         '--events', _events],
                        stdout=subprocess.PIPE
                    ).stdout.decode('utf-8')
                )


class TestDiagnostics(unittest.TestCase):

    def test_completeness(self):
        obs = numpy.array(
            [[4.7, 4.3, numpy.nan, 2.7, 4.1, 5.0]]
        )
        obs_file = NamedTemporaryFile()
        numpy.savetxt(obs_file.name, obs, delimiter=',')

        prd = numpy.array(
            [[5.3, numpy.nan, 5.7, 2.3, 3.3, 4.1],
             [4.3, 4.2, 4.7, 4.3, 3.3, 2.8],
             [5.3, numpy.nan, 5.7, 2.3, 3.8, numpy.nan]]
        )
        prd_file = NamedTemporaryFile()
        numpy.savetxt(prd_file.name, prd, delimiter=',')

        msk = numpy.array(
            [[[True, True, True, False, True, True],
              [True, True, True, True, True, True]],
             [[True, True, True, True, True, False],
              [True, True, True, True, True, True]],
             [[True, True, True, False, False, True],
              [True, True, True, True, True, True]]]
        )
        msk_file = NamedTemporaryFile()
        numpy.savetxt(msk_file.name, msk.reshape(-1, 6), delimiter=',')

        exp = ("{{{ 0.776745},\n"
               "  { 0.701783}},\n"
               " {{ 0.917878},\n"
               "  { 1.281405}},\n"
               " {{ 0.6     },\n"
               "  { 0.450925}}}\n"
               "{{{ 3.},\n"
               "  { 4.}},\n"
               " {{ 4.},\n"
               "  { 5.}},\n"
               " {{ 1.},\n"
               "  { 3.}}}\n")

        res = subprocess.run(
            ['evalhyd', 'evald', obs_file.name, prd_file.name, 'RMSE',
             '--t_msk', msk_file.name, '--diagnostics', 'completeness'],
            stdout=subprocess.PIPE
        ).stdout.decode('utf-8').replace('\r\n', '\n')

        obs_file.close()
        prd_file.close()
        msk_file.close()

        self.assertEqual(res, exp)


if __name__ == '__main__':
    test_loader = unittest.TestLoader()
    test_suite = unittest.TestSuite()

    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestMetrics)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestTransform)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestMasking)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestInterface)
    )
    test_suite.addTests(
        test_loader.loadTestsFromTestCase(TestUncertainty)
    )

    runner = unittest.TextTestRunner(verbosity=2)
    result = runner.run(test_suite)

    if not result.wasSuccessful():
        exit(1)
