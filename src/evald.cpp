#include <array>
#include <string>
#include <unordered_map>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <stdexcept>
#include <memory>
#include <algorithm>
#include <optional>

#include <CLI/CLI.hpp>

#include <xtl/xoptional.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xview.hpp>
#include <xtensor/xstrided_view.hpp>
#include <xtensor/xcsv.hpp>
#include <xtensor/xio.hpp>

#include <evalhyd/evald.hpp>

#include "evalhyd-cli/evald.hpp"

/// Data structure to store flags and options for 'evald' subcommand interface.
struct EvalDInterface
{
    // set default input file names
    std::string obs = "./q_obs.csv";
    std::string prd = "./q_prd.csv";

    std::vector<std::string> metrics;

    bool to_file = false;

    // set default output directory
    std::string output_dir = ".";

    // optionals related to thresholds-based metrics
    std::string thr = {};
    std::optional<std::string> events;

    // optionals related to flow transformation
    std::optional<std::string> transform;
    std::optional<double> exponent;
    std::optional<double> epsilon;

    // optionals related to temporal subsetting
    std::string msk;
    std::string cdt;

    // optionals related to bootstrapping
    std::optional<std::vector<std::string>> bootstrap;
    std::string dts;
    std::optional<int> seed;

    // optionals related to diagnostics
    std::optional<std::vector<std::string>> diags;
};

/// Procedure to run 'evald' subcommand (making use of evalhyd).
void run_evald(EvalDInterface const &opt)
{
    // initialise input and output file streams
    std::ifstream in_file;
    std::ofstream out_file;

    in_file.open(opt.obs);
    xt::xtensor<double, 2> observed = xt::load_csv<double>(in_file);
    in_file.close();

    in_file.open(opt.prd);
    xt::xtensor<double, 2> predicted = xt::load_csv<double>(in_file);
    in_file.close();

    // retrieve dimensions
    std::size_t n_srs = predicted.shape(0);
    std::size_t n_tim = predicted.shape(1);

    // process temporal masks (if provided)
    xt::xtensor<bool, 3> masks;
    xt::xtensor<std::array<char, 32>, 2> conditions;

    if (!opt.msk.empty())
    {
        // read file to determine number of masks
        in_file.open(opt.msk);
        auto n_lin = xt::load_csv<double>(in_file).shape(0);
        in_file.close();

        std::size_t n_msk = 0;
        if (n_lin % n_srs == 0)
        {
            n_msk = n_lin / n_srs;
        }
        else
        {
            throw std::runtime_error(
                    "number of temporal masks is not a multiple of number of "
                    "prediction series"
            );
        }

        // read and reshape CSV data into 3D tensor
        in_file.open(opt.msk);
        masks = xt::reshape_view(
                xt::load_csv<bool>(in_file), {n_srs, n_msk, n_tim}
        );
        in_file.close();
    }
    else if (!opt.cdt.empty())
    {
        // read CSV data
        in_file.open(opt.cdt);
        xt::xtensor<std::string, 2> cdt = xt::load_csv<std::string>(in_file);
        in_file.close();

        // convert string to 32-byte character array
        conditions = xt::xtensor<std::array<char, 32>, 2> (cdt.shape());
        for (std::size_t i = 0; i < cdt.shape(0); i++)
        {
            for (std::size_t j = 0; j < cdt.shape(0); j++)
            {
                auto condition = cdt(i, j);

                std::array<char, 32> arr ({});
                std::copy(condition.begin(), condition.end(), arr.data());

                xt::view(conditions, i, j) = arr;
            }
        }
    }

    // rearrange bootstrap parameters into mapping structure
    std::unordered_map<std::string, int> bootstrap;
    if (opt.bootstrap.has_value())
    {
        if (opt.bootstrap.value().size() % 2)
        {
            throw std::runtime_error(
                    "bootstrap parameters not paired as key/value"
            );
        }
        else
        {
            for (int p = 0; p < opt.bootstrap.value().size() / 2; p++)
            {
                try
                {
                    bootstrap[opt.bootstrap.value()[p*2]] =
                            std::stoi(opt.bootstrap.value()[p*2+1]);
                }
                catch (const std::invalid_argument& e)
                {
                    throw std::runtime_error(
                            "bootstrap parameter value could not be converted to "
                            "an integer for key " + opt.bootstrap.value()[p*2]
                    );
                }
            }
        }
    }

    // initialise (and potentially populate) threshold tensor
    xt::xtensor<double, 2> thresholds = {};

    if (!opt.thr.empty())
    {
        in_file.open(opt.thr);
        thresholds = xt::load_csv<double>(in_file);
        in_file.close();
    }

    // read datetimes in if provided
    std::vector<std::string> datetimes = {};
    if (!opt.dts.empty())
    {
        in_file.open(opt.dts);
        std::string dt;
        while (std::getline(in_file, dt, ','))
        {
            datetimes.push_back(dt);
        }
        in_file.close();
    }

    // compute metrics
    std::vector<xt::xarray<double>> results =
            evalhyd::evald(
                    observed,
                    predicted,
                    opt.metrics,
                    thresholds,
                    (opt.events.has_value())
                        ? opt.events.value()
                        : xtl::missing<std::string>(),
                    (opt.transform.has_value())
                        ? opt.transform.value()
                        : xtl::missing<std::string>(),
                    (opt.exponent.has_value())
                        ? opt.exponent.value()
                        : xtl::missing<double>(),
                    (opt.epsilon.has_value())
                        ? opt.epsilon.value()
                        : xtl::missing<double>(),
                    masks,
                    conditions,
                    (opt.bootstrap.has_value())
                        ? bootstrap
                        : xtl::missing<std::unordered_map<std::string, int>>(),
                    datetimes,
                    (opt.seed.has_value())
                        ? opt.seed.value()
                        : xtl::missing<int>(),
                    (opt.diags.has_value())
                        ? opt.diags.value()
                        : xtl::missing<std::vector<std::string>>()
            );

    // handle output
    int v = 0;

    std::vector<std::string> variables = opt.metrics;
    if (opt.diags.has_value())
    {
        variables.insert(
                variables.end(),
                opt.diags.value().begin(), opt.diags.value().end()
        );
    }

    for (const auto& variable : variables)
    {
        // store results into CSV files
        if(opt.to_file)
        {
            out_file.open(opt.output_dir + "/evald_" += variable + ".csv");
            
            auto result = results[v];
            
            // 3D: (series, subsets, samples)
            if (result.dimension() == 3)
            {
                xt::dump_csv(
                        out_file,
                        xt::reshape_view(
                                result,
                                {result.shape(0) * result.shape(1),
                                 result.shape(2)}
                        )
                );
            }
            // 4D: (series, subsets, samples, components)
            else if (result.dimension() == 4)
            {
                xt::dump_csv(
                        out_file,
                        xt::reshape_view(
                                result,
                                {result.shape(0) * result.shape(1)
                                 * result.shape(2),
                                 result.shape(3)}
                        )
                );
            }
            // 5D: (series, subsets, samples, thresholds, cells)
            else if (result.dimension() == 5)
            {
                xt::dump_csv(
                        out_file,
                        xt::reshape_view(
                                result,
                                {result.shape(0) * result.shape(1)
                                 * result.shape(2) * result.shape(3),
                                 result.shape(4)}
                        )
                );
            }

            out_file.close();
        }
        else
        {
            std::cout << results[v] << std::endl;
        }
        v++;
    }
}

/// Procedure to set up interface for 'evald' subcommand.
void setup_evald(CLI::App &app)
{
    auto opt = std::make_shared<EvalDInterface>();
    auto sub = app.add_subcommand("evald", "Evaluate deterministic predictions");

    // required parameters
    sub->add_option("q_obs", opt->obs,
                    "Path to streamflow observations CSV file")
            ->required()
            ->check(CLI::ExistingFile);
    sub->add_option("q_prd", opt->prd,
                    "Path to streamflow predictions CSV file")
            ->required()
            ->check(CLI::ExistingFile);
    sub->add_option("metrics", opt->metrics,
                    "List of evaluation metrics to compute")
            ->required();

    // optional parameters
    sub->add_option("--q_thr", opt->thr,
                    "Path to streamflow thresholds file")
            ->check(CLI::ExistingFile);

    sub->add_option("--events", opt->events,
                    "Type of streamflow events to consider for threshold \n"
                    "exceedance-based metrics. It can either be set as \n"
                    "'high' (i.e. above threshold) or as 'low' (i.e. below \n"
                    "threshold)");

    sub->add_option("--transform", opt->transform,
                    "Transformation to apply to both streamflow observations \n"
                    "and predictions prior to the calculation of the metrics");

    sub->add_option("--exponent", opt->exponent,
                    "Value of the exponent n to use when the transform is the \n"
                    "power function");

    sub->add_option("--epsilon", opt->epsilon,
                    "Value of the small constant ε to add to both the \n"
                    "streamflow observations and predictions prior to the \n"
                    "calculation of the metrics when the transform is the \n"
                    "reciprocal function, the natural logarithm, or the power \n"
                    "function with a negative exponent");

    sub->add_option("--t_msk", opt->msk,
                    "Path to temporal masks CSV file")
            ->check(CLI::ExistingFile);

    sub->add_option("--m_cdt", opt->cdt,
                    "Path to masking conditions CSV file "
                    "(note: t_msk takes precedence)")
            ->check(CLI::ExistingFile);

    sub->add_option("--bootstrap", opt->bootstrap,
                    "Parameters to configure the bootstrapping in order \n"
                    "to assess the uncertainty in the evaluation metrics");

    sub->add_option("--dts", opt->dts,
                    "Path to datetimes CSV file")
            ->check(CLI::ExistingFile);

    sub->add_option("--seed", opt->seed,
                    "Value of the seed for the random generator");

    sub->add_option("--diagnostics", opt->diags,
                    "List of evaluation diagnostics to compute");

    sub->add_option("--out_dir", opt->output_dir,
                    "Path to output directory")
            ->check(CLI::ExistingDirectory);

    // flags
    sub->add_flag("--to_file", opt->to_file,
                  "Divert output to CSV file, otherwise output to console");

    sub->callback([opt]() { run_evald(*opt); });
}
