#include <map>
#include <vector>
#include <array>
#include <string>
#include <set>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <numeric>
#include <algorithm>
#include <optional>

#include <CLI/CLI.hpp>

#include <xtl/xoptional.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xview.hpp>
#include <xtensor/xstrided_view.hpp>
#include <xtensor/xcsv.hpp>
#include <xtensor/xio.hpp>

#include <evalhyd/evalp.hpp>

#include "evalhyd-cli/evalp.hpp"

namespace fs = std::filesystem;

typedef std::map<std::string, std::string> s_tree;
typedef std::map<std::string, std::map<std::string, std::string>> ls_tree;

/// Function to create tree of data for each site.
s_tree find_s_data(const std::string& path)
{
    s_tree tree;

    for (const auto & site : fs::directory_iterator(path))
    {
        if (site.path().extension().string() != ".csv")
            throw std::runtime_error(
                    "observation files must feature '.csv' as extension"
            );

        tree[site.path().stem().string()] = site.path().string();
    }

    return tree;
}

/// Function to create tree of data for each leadtime and for each site.
ls_tree find_ls_data(const std::string& path)
{
    ls_tree tree;

    for (const auto & leadtime : fs::directory_iterator(path))
    {
        for (const auto & site : fs::directory_iterator(leadtime.path()))
        {
            if (site.path().extension().string() != ".csv")
                throw std::runtime_error(
                        "prediction files must feature '.csv' as extension"
                );

            tree[leadtime.path().stem().string()][site.path().stem().string()] =
                    site.path().string();
        }
    }

    return tree;
}

/// Function to check tree of input files match between observations and
/// predictions, and, if so, return lists of leadtimes and sites.
std::vector<std::vector<std::string>> check_obs_vs_prd(
        const s_tree& obs, const ls_tree& prd
)
{
    // process sites from observation data
    std::vector<std::string> sites;
    for (const auto &[site, path]: obs)
        sites.push_back(site);

    // process leadtimes and sites from prediction data
    // and check that sites are consistent with observation data
    std::vector<std::string> leadtimes;
    for (const auto & [ leadtime, map ] : prd)
    {
        leadtimes.push_back(leadtime);

        // collect all sites for leadtime
        std::vector<std::string> s;
        for (const auto &[site, path]: map)
            s.push_back(site);

        // check that sites of current leadtime are consistent with reference
        if (s != sites)
            throw std::runtime_error(
                    "sites not matching with observations for leadtime "
                    + leadtime
            );

    }

    return {leadtimes, sites};
}

// Procedure to check that all leadtimes and all sites are in tree data structure
void check_sites_in_ls_tree(
        const std::vector<std::string>& l,
        const std::vector<std::string>& s,
        ls_tree& tree, std::string var
)
{
    for (const auto& leadtime : l)
    {
        if (tree.find(leadtime) == tree.end())
            throw std::runtime_error(
                    "missing lead time " + leadtime + "for " + var
        );

        for (const auto& site : s)
        {
            if (tree[leadtime].find(site) == tree[leadtime].end())
                throw std::runtime_error(
                        "missing site " + site + "for "
                        + leadtime + "and for " + var
                );
        }
    }

}

// Procedure to check that all sites are in tree data structure
void check_sites_in_s_tree(
        const std::vector<std::string>& s, const s_tree& tree, std::string var
)
{
    for (const auto& site : s)
    {
        if (tree.find(site) == tree.end())
            throw std::runtime_error(
                    "missing site " + site + "for " + var
            );
    }
}

/// Data structure to store flags and options for 'evalp' subcommand interface.
struct EvalPInterface
{
    // set default input folder paths
    std::string obs = "./q_obs";
    std::string prd = "./q_prd";

    std::vector<std::string> metrics;

    bool to_file = false;

    // set default output directory
    std::string output_dir = ".";

    // optionals related to thresholds-based metrics
    std::string thr = {};
    std::optional<std::string> events;

    // optionals related to intervals-based metrics
    std::vector<double> lvl = {};

    // optionals related to temporal subsetting
    std::string msk;
    std::string cdt;

    // optionals related to bootstrapping
    std::optional<std::vector<std::string>> bootstrap;
    std::string dts;
    std::optional<int> seed;

    // optionals related to diagnostics
    std::optional<std::vector<std::string>> diags;
};

/// Procedure to run 'evalp' subcommand (making use of evalhyd).
void run_evalp(EvalPInterface const &opt)
{
    // initialise input and output file streams
    std::ifstream in_file;
    std::ofstream out_file;

    // process input data (observations and predictions)
    s_tree obs_t = find_s_data(opt.obs);
    ls_tree prd_t = find_ls_data(opt.prd);

    std::vector<std::vector<std::string>> checks = check_obs_vs_prd(obs_t,
                                                                    prd_t);

    std::vector<std::string> leadtimes = checks[0];
    std::vector<std::string> sites = checks[1];

    // process optional input data (thresholds, masks)
    s_tree thr_t;
    if (!opt.thr.empty())
    {
        thr_t = find_s_data(opt.thr);
        check_sites_in_s_tree(sites, thr_t, "streamflow thresholds");
    }

    ls_tree msk_t;
    if (!opt.msk.empty())
    {
        msk_t = find_ls_data(opt.msk);
        check_sites_in_ls_tree(leadtimes, sites, msk_t, "temporal masks");
    }

    s_tree cdt_t;
    if (!opt.cdt.empty())
    {
        cdt_t = find_s_data(opt.cdt);
        check_sites_in_s_tree(sites, cdt_t, "masking conditions");
    }

    // use first prediction file to determine problem inner dimensions
    // (i.e. ensemble members and time)
    in_file.open(prd_t[leadtimes[0]][sites[0]]);
    auto inner_shp = xt::load_csv<double>(in_file).shape();
    in_file.close();

    // populate tensors with input data
    xt::xtensor<double, 2> observations = xt::zeros<double>(
            {sites.size(), inner_shp[1]}
    );
    xt::xtensor<double, 4> predictions = xt::zeros<double>(
            {sites.size(), leadtimes.size(), inner_shp[0], inner_shp[1]}
    );

    int s = 0;
    for (const auto & st : sites)
    {
        in_file.open(obs_t[st]);
        auto observation = xt::view(xt::load_csv<double>(in_file), 0);
        if (observation.size() != inner_shp[1])
        {
            in_file.close();
            throw std::runtime_error(
                    "mismatch between observations and predictions dimensions"
            );
        }
        xt::view(observations, s, xt::all()) = observation;
        in_file.close();
        s++;
    }

    int l = 0;
    for (const auto & lt : leadtimes)
    {
        s = 0;
        for (const auto & st : sites)
        {
            in_file.open(prd_t[lt][st]);
            auto prediction = xt::load_csv<double>(in_file);
            if (prediction.shape() != inner_shp)
            {
                in_file.close();
                throw std::runtime_error(
                        "mismatch in predictions dimensions across leadtimes"
                        "and/or sites"
                );
            }
            xt::view(predictions, s, l, xt::all(), xt::all()) = prediction;
            in_file.close();
            s++;
        }
        l++;
    }

    // process temporal masks (if provided)
    xt::xtensor<bool, 4> masks;
    xt::xtensor<std::array<char, 32>, 2> conditions;

    std::size_t n_msk = 0;

    if (!opt.msk.empty())
    {
        // (use first threshold file to determine number of masks)
        in_file.open(msk_t[leadtimes[0]][sites[0]]);
        n_msk = xt::load_csv<double>(in_file).shape(0);
        in_file.close();

        masks = xt::zeros<bool>({sites.size(), leadtimes.size(), n_msk, inner_shp[1]});

        l = 0;
        for (const auto & lt : leadtimes)
        {
            s = 0;
            for (const auto &st: sites)
            {
                in_file.open(msk_t[lt][st]);
                auto mask = xt::load_csv<bool>(in_file);
                if (mask.shape(0) != n_msk)
                {
                    in_file.close();
                    throw std::runtime_error(
                            "mismatch in number of temporal masks across sites"
                    );
                }
                xt::view(masks, s, l, xt::all(), xt::all()) = mask;
                in_file.close();
                s++;
            }
            l++;
        }
    }
    else if (!opt.cdt.empty())
    {
        // (use first masking conditions file to determine number of masks)
        in_file.open(cdt_t[sites[0]]);
        std::string line;
        while (std::getline(in_file, line))
        {
            n_msk++;
        }
        in_file.close();

        conditions = xt::xtensor<std::array<char, 32>, 2> ({sites.size(), n_msk});

        s = 0;
        for (const auto & st : sites)
        {
            in_file.open(cdt_t[st]);
            int ss = 0;
            while (std::getline(in_file, line))
            {
                // remove special characters carriage return/new line
                line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());
                line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());

                // convert string to 32-byte character array
                std::array<char, 32> arr ({});
                std::copy(line.begin(), line.end(), arr.data());

                xt::view(conditions, s, ss) = arr;
                ss++;
            }
            in_file.close();
            if (ss != n_msk)
            {
                throw std::runtime_error(
                        "mismatch in number of masking conditions across sites"
                );
            }
            s++;
        }
    }
    else
    {
        n_msk = 1;
    }

    // initialise (and potentially populate) threshold tensor
    xt::xtensor<double, 2> thresholds = {};

    if (!opt.thr.empty())
    {
        // (use first threshold file to determine number of thresholds)
        in_file.open(thr_t[sites[0]]);
        auto n_thr = xt::load_csv<double>(in_file).shape(1);
        in_file.close();

        thresholds = xt::zeros<double>({sites.size(), n_thr});

        s = 0;
        for (const auto & st : sites)
        {
            in_file.open(thr_t[st]);
            auto threshold = xt::view(xt::load_csv<double>(in_file), 0);
            if (threshold.size() != n_thr)
            {
                in_file.close();
                throw std::runtime_error(
                        "mismatch in number of thresholds across sites"
                );
            }
            xt::view(thresholds, s, xt::all()) = threshold;
            in_file.close();
            s++;
        }
    }

    // rearrange bootstrap parameters into mapping structure
    std::unordered_map<std::string, int> bootstrap;
    if (opt.bootstrap.has_value())
    {
        if (opt.bootstrap.value().size() % 2)
        {
            throw std::runtime_error(
                    "bootstrap parameters not paired as key/value"
            );
        }
        else
        {
            for (int p = 0; p < opt.bootstrap.value().size() / 2; p++)
            {
                try
                {
                    bootstrap[opt.bootstrap.value()[p*2]] =
                            std::stoi(opt.bootstrap.value()[p*2+1]);
                }
                catch (const std::invalid_argument& e)
                {
                    throw std::runtime_error(
                            "bootstrap parameter value could not be converted to "
                            "an integer for key " + opt.bootstrap.value()[p*2]
                    );
                }
            }
        }
    }

    // read datetimes in if provided
    std::vector<std::string> datetimes = {};
    if (!opt.dts.empty())
    {
        in_file.open(opt.dts);
        std::string dt;
        while (std::getline(in_file, dt, ','))
        {
            datetimes.push_back(dt);
        }
        in_file.close();
    }

    // compute metrics
    std::vector<xt::xarray<double>> results =
            evalhyd::evalp(
                    observations,
                    predictions,
                    opt.metrics,
                    thresholds,
                    (opt.events.has_value())
                        ? opt.events.value()
                        : xtl::missing<std::string>(),
                    opt.lvl,
                    masks,
                    conditions,
                    (opt.bootstrap.has_value())
                        ? bootstrap
                        : xtl::missing<std::unordered_map<std::string, int>>(),
                    datetimes,
                    (opt.seed.has_value())
                        ? opt.seed.value()
                        : xtl::missing<int>(),
                    (opt.diags.has_value())
                        ? opt.diags.value()
                        : xtl::missing<std::vector<std::string>>()
            );

    // handle output
    int v = 0;

    std::vector<std::string> variables = opt.metrics;
    if (opt.diags.has_value())
    {
        variables.insert(
                variables.end(),
                opt.diags.value().begin(), opt.diags.value().end()
        );
    }

    std::vector<std::string> multisite = {"ES"};

    // define lambda function to write result to CSV file
    auto write_to_file = [&out_file](auto res)
    {
        // 2D: (subsets, samples)
        if (res.dimension() == 2)
        {
            xt::dump_csv(out_file, res);
        }
        // 3D: (subsets, samples, thresholds)
        //     or (subsets, samples, quantiles)
        //     or (subsets, samples, ranks)
        //     or (subsets, samples, intervals)
        else if (res.dimension() == 3)
        {
            xt::dump_csv(
                    out_file,
                    xt::reshape_view(
                            res,
                            {res.shape(0) * res.shape(1),
                             res.shape(2)}
                    )
            );
        }
        // 4D: (subsets, samples, thresholds, components)
        //     or (subsets, samples, levels, thresholds)
        else if (res.dimension() == 4)
        {
            xt::dump_csv(
                    out_file,
                    xt::reshape_view(
                            res,
                            {res.shape(0) * res.shape(1)
                             * res.shape(2),
                             res.shape(3)}
                    )
            );
        }
        // 5D: (subsets, samples, thresholds, bins, axes)
        //     (subsets, samples, levels, thresholds, cells)
        else if (res.dimension() == 5)
        {
            xt::dump_csv(
                    out_file,
                    xt::reshape_view(
                            res,
                            {res.shape(0) * res.shape(1)
                             * res.shape(2) * res.shape(3),
                             res.shape(4)}
                    )
            );
        }
        else
        {
            throw std::runtime_error(
                    "metric to output features more than five "
                    "dimensions"
            );
        }
    };

    for (const auto& variable : variables)
    {
        // store results into CSV files (one for each leadtime and site)
        if(opt.to_file)
        {
            l = 0;
            for (const auto & lt : leadtimes)
            {
                fs::path path = {opt.output_dir + "/" + lt};
                fs::create_directories(path);

                if (std::find(multisite.begin(), multisite.end(), variable)
                    != multisite.end())
                {
                    out_file.open(path.string() + "/" + "multisite_" + variable + ".csv");
                    write_to_file(xt::view(results[v], 0, l));
                    out_file.close();
                }
                else
                {
                    s = 0;
                    for (const auto &st: sites)
                    {
                        out_file.open(path.string() + "/" + st + "_" + variable + ".csv");
                        write_to_file(xt::view(results[v], s, l));
                        out_file.close();
                        s++;
                    }
                }
                l++;
            }
        }
        // direct results to terminal
        else
        {
            std::cout << results[v] << std::endl;
        }
        v++;
    }
}

/// Procedure to set up interface for 'evalp' subcommand.
void setup_evalp(CLI::App &app)
{
    auto opt = std::make_shared<EvalPInterface>();
    auto sub = app.add_subcommand("evalp", "Evaluate probabilistic predictions");

    // required parameters
    sub->add_option("q_obs", opt->obs,
                    "Path to streamflow observations folder")
            ->required()
            ->check(CLI::ExistingDirectory);

    sub->add_option("q_prd", opt->prd,
                    "Path to streamflow predictions folder")
            ->required()
            ->check(CLI::ExistingDirectory);

    sub->add_option("metrics", opt->metrics,
                    "List of evaluation metrics to compute")
            ->required();

    // optional parameters
    sub->add_option("--q_thr", opt->thr,
                    "Path to streamflow thresholds folder")
            ->check(CLI::ExistingDirectory);

    sub->add_option("--events", opt->events,
                    "Type of streamflow events to consider for threshold \n"
                    "exceedance-based metrics. It can either be set as \n"
                    "'high' (i.e. above threshold) or as 'low' (i.e. below \n"
                    "threshold)");

    sub->add_option("--c_lvl", opt->lvl,
                    "Confidence levels to use for intervals-based metrics");

    sub->add_option("--t_msk", opt->msk,
                    "Path to temporal masks folder")
            ->check(CLI::ExistingDirectory);

    sub->add_option("--m_cdt", opt->cdt,
                    "Path to masking conditions folder "
                    "(note: t_msk takes precedence)")
            ->check(CLI::ExistingDirectory);

    sub->add_option("--bootstrap", opt->bootstrap,
                    "Parameters to configure the bootstrapping in order \n"
                    "to assess the uncertainty in the evaluation metrics");

    sub->add_option("--dts", opt->dts,
                    "Path to datetimes CSV file")
            ->check(CLI::ExistingFile);

    sub->add_option("--seed", opt->seed,
                    "Value of the seed for the random generator");

    sub->add_option("--diagnostics", opt->diags,
                    "List of evaluation diagnostics to compute");

    sub->add_option("--out_dir", opt->output_dir,
                    "Path to output directory")
            ->check(CLI::ExistingDirectory);

    // flags
    sub->add_flag("--to_file", opt->to_file,
                  "Divert output to CSV file, otherwise output to console");

    sub->callback([opt]() { run_evalp(*opt); });
}
